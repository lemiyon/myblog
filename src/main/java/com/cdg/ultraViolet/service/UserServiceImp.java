package com.cdg.ultraViolet.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cdg.ultraViolet.domain.User;
import com.cdg.ultraViolet.mapper.UserMapper;

@Service //여기에 등록을 안해 주면 스프링이 이 서비스를 못찾고 NoSuchBean오류를 낸다.
public class UserServiceImp implements UserService {

	@Autowired
	UserMapper mapper;
	
	@Override
	public void login(User user) throws LoginFailureException{
		// TODO Auto-generated method stub
		User trueUser = mapper.selectUser(user);
		if(trueUser == null)
		{
			//만약 해당 user가 존재하지 않으면, 로그인 실패 예외를 발생시킨다. 
			//어디서 듣기로는 일부러 이렇게 분기를 때리는 경우도 있다
			//는 안티 리버싱 기법. 
			//디버깅 중이면 디버거가 해당 예외를 처리하게 되는데, 이땜시 디버거를 이용 리버싱 중 원하는 코드로 점프할 수 가 없다.
			throw new LoginFailureException();
		}
		
	}
	@Override
	public void signUp(User user) {
		// TODO Auto-generated method stub
		mapper.insertUser(user);
	}
}
