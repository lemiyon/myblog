package com.cdg.ultraViolet.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.cdg.ultraViolet.domain.User;
import com.cdg.ultraViolet.service.LoginFailureException;
import com.cdg.ultraViolet.service.UserService;

/*
 * application-context에서 Controller가 빠지는 이유....
 * Spring은 오직 웹만을 위한 프레임워크가 아니다. 따라서 Web의 컨트롤러들은 웹에 묶여있는 애들이기 때문에 나중에
 * 다른 환경에서 돌아가는 데 방해가 될 수 있다.
 * */
 

@Controller
@SessionAttributes("user_id")
public class MainController {

	@Autowired
	 UserService service;
	
	@RequestMapping("/index")
	public String index()
	{
		return "/index";
	}
	
	@RequestMapping("/loginForm")
	public String loginForm()
	{
		return "/loginForm";
	}
	
	@RequestMapping("/login")
	public String login(Model model, User user) throws LoginFailureException
	{
		service.login(user);
		model.addAttribute("user_id", user.getId());
		return "redirect:/board/list";
	}
	
	@ExceptionHandler(value = LoginFailureException.class)
	public String loginFail()
	{
		System.out.println("LoginFailureException has occured");
		return "redirect:/loginForm";
	}
	
	@RequestMapping("/signUpForm")
	public String signUpForm()
	{
		return "/signUpForm";
	}
	
	@RequestMapping(path = "/signUp" , method = RequestMethod.POST)
	public String signUp(User user)
	{
		service.signUp(user);
		return "redirect:/loginForm";
	}
}
