package com.cdg.ultraViolet.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

/**
 * 로그인 구현을 위해서 존재하는 인터셉터
 * 필터와는 많이 다르다. 
 * 그 차이는 원노트에서 참고할 거 
 * 
 * Servlet Filter implementation class LoginInterceptor
 */


public class LoginInterceptor extends HandlerInterceptorAdapter{
	
	
//preHandle : Controller 가 수행되기 전에 실행됩니다. 여기서는 이후 Controller를 수행할지 여부를 boolean 으로 return 하게 됩니다.


  @Override
public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
		throws Exception {
	// TODO Auto-generated method stub
	  System.out.println("you just entered in Interceptor. it's preHandle");
	  
	  if(request.getSession() == null)
	  {
		  response.sendRedirect("./loginForm");
	  }
	  
	return true;
}
//	postHandle : Controller 가 수행된후 View 를 호출하기 전 상태입니다.
  @Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		// TODO Auto-generated method stub
		super.postHandle(request, response, handler, modelAndView);
		
		  System.out.println("you just entered in Interceptor. it's postHandle");
	}
  
  
//	afterCompletion : View 작업까지 완료된 후 호출 됩니다. responseBody 를 이용할 경우 UI 에 이미 값을 전달후 해당 부분이 호출됩니다.	
  @Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
		// TODO Auto-generated method stub
		super.afterCompletion(request, response, handler, ex);
		  System.out.println("you just entered in Interceptor. it's afterCompletion");
	}
}
