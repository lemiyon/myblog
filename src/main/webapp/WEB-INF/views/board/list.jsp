<% response.setHeader("Pragma", "no-cache"); //HTTP 1.0
response.setHeader("Cache-Control", "no-cache"); //HTTP 1.1
response.setHeader("Cache-Control", "no-store"); //HTTP 1.1
response.setDateHeader("Expires", 0L); // Do not cache in proxy server
 %>
 <!-- 서버 사이드 스크립트는 가장 위에서 돌리는 게 좋다 -->
 <!-- 위의 서버 사이드 스크립트는 뒤로 가기 했을 때, 이전에 저장한 캐시가 아니라 새로 받아오도록 강제하는 것이다. 이래야 hit 증가가 뒤로 가기를 눌러도 반영된다. -->
<!-- 여기의 브라우저로는 안돌아가고크롬에서는 잘 돌아간다. 브라우저의 백 버튼이 history.back()이 아닐 수도 있따. -->
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>  
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Welcome to UltraVioloet</title>
</head>
<body>
	<h1>게시판 목록</h1>
	<h2>welcome, ${sessionScope.user_id}</h2>
	<table>
		<tr>
			<td>제목</td>
			<td>내용</td>
			<td>작성자</td>
			<td>조회수</td>
		</tr>
		
		<c:forEach items="${boardList}" var="board">
			<tr>
				<td><a href="./detail?boardNo=${board.boardNo}">${board.title}</a></td>
				<td>${board.contents}</td>
				<td>${board.userId}</td>
				<td>${board.hit}</td>
		</tr>
		</c:forEach>
	</table>
	<input type="submit" id="btnSubmit" value="submit"
		onclick="location.href='./writeForm'">
<!-- onclick 안에 저거는 자바스크립트. <a>와 똑같은 효과를준다. -->

</body>
</html>