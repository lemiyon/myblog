package com.cdg.ultraViolet.mapper;

import com.cdg.ultraViolet.domain.User;

public interface UserMapper {

	User selectUser(User user);
	void insertUser(User user);
	void deleteUser(String userId);
}
