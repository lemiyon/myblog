package com.cdg.ultraViolet.service;

import com.cdg.ultraViolet.domain.User;

public interface UserService {
	void login(User user) throws LoginFailureException;

	void signUp(User user);
}
